# TeXFormat
TeXの使い方は以下を参考にしてみてください~
https://utkhii.px.tsukuba.ac.jp/internal/lab/index.php?ALICE/ALICE_TSUKUBA_2020-/ALICE_LaTeX%282020%29

bibTexを使うためには以下のコマンドをAbstract/Paperディレクトリの中で実行してください.
```
platex main
pbibtex main
platex main
platex main
dvipdfmx main
```


